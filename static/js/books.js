var bookmarked = [];

$(document).ready(function () {

    updateCounter();

    $('body').on('click', '.favorite', function () {
        var idFound = favs.find(obj => obj.id === $(this).attr('book-id'));
        if (!idFound) {
            favs.push({
                'id': $(this).attr('book-id')
            });
            $(this).attr('src', like);
            updateCounter();
        } else {
            var indexOfId = favs.indexOf(idFound);
            favs.splice(indexOfId, 1);
            $(this).attr('src', dislike);
            updateCounter();
        }
    });

    function updateCounter() {
        $('.favorite-counter').text(favs.length);
    }

});
