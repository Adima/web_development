import json

from django.shortcuts import render
from django.http import HttpResponse

BOOK_API = 'https://www.googleapis.com/books/v1/volumes?q=quilting'

def index(request):
    return render(request, 'MyStory/index.html', {})

def projects(request):  
    return render(request, 'MyStory/projects.html', {})

def books(request):
    return render(request, 'MyStory/books.html', {})

def get_book(request):
    data = request.get(BOOK_API).json()
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')

def registration(request):
    return render(request, 'MyStory/registration.html', {})
