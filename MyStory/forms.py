from django import forms

from .models import Member


class RegistrationForm(forms.ModelForm):

    password = forms.CharField(
        label='Password',
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        help_text='Please enter your password',
        required=True,
    )

    confirm = forms.CharField(
        label='Password Confirmation',
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        help_text='Confirm Password',
        required=True,
    )

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)

        name_field = self.fields['name']
        name_field.label = 'Nama'
        name_field.widget = forms.TextInput(attrs={'class': 'form-control',
                                                   'placeholder': 'Masukkan nama Anda'})
        name_field.required = True

        email_field = self.fields['email']
        email_field.help_text = 'You have to have an email'
        email_field.widget = forms.EmailInput(attrs={'class': 'form-control',
                                                     'placeholder': 'Masukkan e-mail Anda'})
    def save(self, commit=True):
        subscriber = super().save(commit=False)
        subscriber.password = self.cleaned_data['password']
        if commit:
            subscriber.save()
        return subscriber

    class Meta:
        model = Member
        fields = ['name', 'email']